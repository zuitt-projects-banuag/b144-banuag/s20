//JSON OBJECTS
//JSON stands fro JavaScript Object Notation
//JSON is used for serializing different data types into bytes.
//Serialization is the process of converting data into a series of bytes for easier transmission/transfer of information
//byte =? binary digits (1 and 0) that is used to represent a character.

/*
JSON Data Format
	Syntax:
		{
			"propertyA": "valueA",
			"propertyB": "valueB",
		}
*/

/*
JS Object
{
	city: "QC",
	province: "Metro Manila",
	country: "Philippines"
}

JSON - have a string ("" and '')
Binabasa ng JSON into string
{
	"city": "QC",
	"province": "Metro Manila",
	"country": "Philippines"
}

JSON ARRAY

"cities": [
	
	{
		"city": "QC",
		"province": "Metro Manila",
		"country": "Philippines"
	},
	{
		"city": "QC",
		"province": "Metro Manila",
		"country": "Philippines"
	},
	{
		"city": "QC",
		"province": "Metro Manila",
		"country": "Philippines"
	}

];
*/

//JSON METHODS
//The JSON contains methods for parsing and converting data into stringified JSON

/*
	- Stringified JSON is a JavaScript object converted into a string to be used in other function of a JavaScript application
*/

//SENDING DATA
// Object to JSON used JSON.stringify
let batchesArr = [
	{
		batchName: 'Batch X'
	},
	{
		batchName: 'Batch Y'
	}
]

console.log('Result from stringify method')
//The 'stringify' method is used to convert JS Object into JSON(string) 
console.log(JSON.stringify(batchesArr))


let data = JSON.stringify(
		{
			name: 'John',
			age: 31,
			address: {
				city: 'Manila',
				country: 'Philippines'
			}
		}
	)
console.log(data)

let firstName = prompt("First Name:");
let lastName = prompt("Last Name:");
let age = prompt("Age:");
let address = prompt("Address:");

let userDetails = JSON.stringify(
		{
			firstName: firstName,
			lastName: lastName,
			age: age,
			address: address
		}
	)
console.log(userDetails)

//Converting Stringified JSON into JS Objects
//Information is commonly sent to application in stringified JSON and then converted back into objects.
//This happens both for sending information to a backendd app and sending information back to frontend app
//Upon receiving data, JSON text can be converted to a JS object with parse
//Kapag naka JSON galing sa computer yung data

//RECEIVING DATA
let batchesJSON = `[
	{
 		"batchName": "Batch X"
	},
	{
 		"batchName": "Batch Y"
	}
]`

console.log('Result from parse method:')
console.log(JSON.parse(batchesJSON))

let stringifiedObject = `[
	{
		"name": "John",
		"age": "31",
		"address": {
			"city": "Manila",
			"country": "Philippines"
		}
	}
]`

console.log('Result from parse method:')
console.log(JSON.parse(stringifiedObject))
